# README #

Once you have node.js installed, run `node index.js` and browse to the website.

Use `runTests.sh` to run the JavaScript test harness.

### What is this repository for? ###

* A proof of concept for zero-install grid computing

### Who do I talk to? ###

* [Craig Nicol's Blog posts about this project](http://craignicol.wordpress.com/category/development/nmandelbrot-development/)
* [Craig Nicol on Google+](http://plus.google.com/+CraigNicolGeek)
* [Craig Nicol on Twitter](http://www.twitter.com/craignicol)