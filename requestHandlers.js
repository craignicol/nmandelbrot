var staticHandler = require("./static"),
    querystring = require("querystring"),
    cache = require('memory-cache'),
    mandeljob = require('nmandelbrot/mandeljob');

function staticContent(data, response) {
  staticHandler.returnContent(data, response);
}

function redirectIndex(data, response) {
    console.log("Redirecting...");
      response.writeHead(301, {"Location": "/static/index.html"});
      response.write("Redirecting from : " + data);
      response.end();    
}

function sendCache(data, response) {
    response.writeHead(200, {"Content-Type": 'text/javascript'}); 
    response.write('(');
    response.write("("+cache.dumpJSON()+")");
    response.write(')');
    response.end();    
}

function sendMoreData(data, response) {
//    console.log("data = " + JSON.stringify(data))
    response.writeHead(200, {"Content-Type": 'text/javascript'}); 
    response.write('(');
    response.write(mandeljob.calculateNext(data));
    response.write(')');
    response.end();
}

exports.staticContent = staticContent;
exports.redirectIndex = redirectIndex;
exports.sendMoreData = sendMoreData;
exports.sendCache = sendCache;
