function route(handle, uri, response, content) {
  var handler = '', data = {'location' : '', 'query': uri.query, 'content' : content};
  var path = uri.pathname.split('/');
  if (path.length > 1) {
      handler = "/" + path[1]; // [0] is the empty root
      data.location = path[2];
  }

//  console.log("About to route a request for '" + JSON.stringify(uri) + "' to '" + handler + "'");

if (typeof handle[handler] === 'function') {
    handle[handler](data, response);
  } else {
    console.log("No request handler found for " + JSON.stringify(uri) + " (found '" + JSON.stringify(handle[handler]) + "' instead)");
    response.writeHead(404, {"Content-Type": "text/plain"});
    response.write("404 Not found");
    response.end();
  }
}

exports.route = route;
