var server = require("./server");
var router = require("./router");
var requestHandlers = require("./requestHandlers");

var handle = {};
handle["/"] = requestHandlers.redirectIndex;
handle["/static"] = requestHandlers.staticContent;
handle["/data"] = requestHandlers.sendMoreData;
handle["/cache"] = requestHandlers.sendCache;

server.start(router.route, handle);
