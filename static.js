var path = require("path"),
    fs = require("fs");

var returnContent = function(data, response) {
    var filename =  path.join(process.cwd(), path.join("static", data.location));
          path.exists(filename, function(exists) {
        if(!exists) {
          response.writeHead(404, {"Content-Type": "text/plain"});
          response.write("404 Not Found\n");
          console.log("404 - Requested : " + filename + " aka " + filename.split("/"));
          response.end();
          return;
        }
    
        if (fs.statSync(filename).isDirectory()) filename += '/index.html';
    
        fs.readFile(filename, "binary", function(err, file) {
          if(err) {        
            response.writeHead(500, {"Content-Type": "text/plain"});
            response.write(err + "\n");
            response.end();
            return;
          }
          
        var extname = path.extname(filename);
        var contentType = 'text/html';
        switch (extname) {
            case '.js':
                contentType = 'text/javascript';
                break;
            case '.css':
                contentType = 'text/css';
                break;
        }      
          
          response.writeHead(200, {"Content-Type": contentType});
          response.write(file, "binary");
          response.end();
        
        });
      });
  };

  
  exports.returnContent = returnContent;
