var http = require("http");
var url = require("url");
var port = process.env.C9_PORT || 8000;
var cache = require("memory-cache");

function start(route, handle) {
  function onRequest(request, response) {
    var uri = url.parse(request.url);
    //console.log("Request for " + uri.pathname + " received.");

    if (request.method == 'POST') {
        console.log("[200] " + request.method + " to " + request.url);
        var fullBody = '';
        request.on('data', function(chunk) {
            // append the current chunk of data to the fullBody variable
            fullBody += chunk.toString();
        });
        request.on('end', function() {
            route(handle, uri, response, fullBody);
        });
    } else {    
        route(handle, uri, response, '');
    }
  }

  http.createServer(onRequest).listen(parseInt(port, 10));
  console.log("Server has started on port:" + port + ".");
  
  cache.restoreJSON();
  console.log("Restoring cache.");
}

exports.start = start;
